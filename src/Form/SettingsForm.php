<?php declare(strict_types=1);

namespace Drupal\permission_watchdog\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\Role;

/**
 * Configure Permission watchdog settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'permission_watchdog_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['permission_watchdog.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['all_roles'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Track all roles for changes to permissions'),
      '#default_value' => $this->config('permission_watchdog.settings')->get('all_roles'),
    ];

    $form['roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Track changes to permissions on the following roles'),
      '#description' => $this->t('Administrative roles are not present in this list because they have all permissions implicitly, e.g. without actually storing the whole list. So it is impossible to compare the changes. Also, no changes to their permissions could be done in the UI.'),
      '#default_value' => $this->config('permission_watchdog.settings')->get('roles'),
      // Admin roles don't store permissions,  so we would have nothing to compare
      // against.
      '#options' => array_filter(user_role_names(), function ($role_id) {
        $role = Role::load($role_id);
        return !$role->isAdmin();
      }, ARRAY_FILTER_USE_KEY),
      '#states' => [
        'visible' => [
          ':input[name="all_roles"]' => ['checked' => FALSE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('all_roles')) {
      $form_state->setValue('roles', []);
    }
    elseif (empty(array_filter($form_state->getValue('roles')))) {
      $form_state->setErrorByName('roles', $this->t('At least one role must be selected'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('permission_watchdog.settings')
      ->set('all_roles', $form_state->getValue('all_roles'))
      ->set('roles', array_values(array_filter($form_state->getValue('roles'))))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
