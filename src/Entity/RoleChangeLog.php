<?php declare(strict_types=1);

namespace Drupal\permission_watchdog\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\permission_watchdog\RoleChangeLogInterface;

/**
 * Defines the role change log entity class.
 *
 * @ContentEntityType(
 *   id = "role_change_log",
 *   label = @Translation("Role change log"),
 *   label_collection = @Translation("Role change logs"),
 *   label_singular = @Translation("role change log"),
 *   label_plural = @Translation("role change logs"),
 *   label_count = @PluralTranslation(
 *     singular = "@count role change logs",
 *     plural = "@count role change logs",
 *   ),
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData"
 *   },
 *   base_table = "role_change_log",
 *   entity_keys = {
 *     "id" = "id",
 *   }
 * )
 */
class RoleChangeLog extends ContentEntityBase implements RoleChangeLogInterface {

  /**
   * Permission was added to role.
   */
  const PERMISSION_ADDED = 'added';

  /**
   * Permission was added to role.
   */
  const PERMISSION_REMOVED = 'removed';

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['role'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Changed role'))
      ->setSetting('target_type', 'user_role')
      ->addConstraint('NotBlank')
      ->setStorageRequired(TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User who made the change'))
      ->setStorageRequired(TRUE)
      ->addConstraint('NotBlank')
      ->setSetting('target_type', 'user');

    $fields['timestamp'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Time action performed'))
      ->setDescription(t('The time that the role change log was created.'))
      ->setStorageRequired(TRUE)
      ->addConstraint('NotBlank')
      ->setDefaultValueCallback(static::class . '::getDefaultTimestamp');

    $fields['actions'] = BaseFieldDefinition::create('permission_action')
      ->setLabel(t('Permission actions'))
      ->setDescription(t('Actions performed on the role'))
      ->setStorageRequired(TRUE)
      ->addConstraint('NotBlank')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);

    return $fields;
  }

  /**
   * Returns current system time.
   *
   * @return int
   *   Current time as a Unix timestamp.
   */
  public static function getDefaultTimestamp(): int {
    return \time();
  }

}
