<?php

namespace Drupal\permission_watchdog;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a role change log entity type.
 */
interface RoleChangeLogInterface extends ContentEntityInterface {

}
