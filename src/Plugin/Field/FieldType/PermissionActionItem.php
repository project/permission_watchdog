<?php declare(strict_types=1);

namespace Drupal\permission_watchdog\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\permission_watchdog\Entity\RoleChangeLog;

/**
 * Defines the 'permission_action' field type.
 *
 * @FieldType(
 *   id = "permission_action",
 *   label = @Translation("Permission action"),
 *   description = @Translation("An entity field for storing role permissions change."),
 *   no_ui = TRUE,
 *   list_class = "\Drupal\Core\Field\MapFieldItemList",
 * )
 */
class PermissionActionItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    if ($this->permission !== NULL) {
      return FALSE;
    }
    elseif ($this->action !== NULL) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    // There is no main property, both are important.
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['permission'] = DataDefinition::create('string')
      ->setLabel(t('Permission'));

    $properties['action'] = DataDefinition::create('string')
      ->setLabel(t('Action'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();

    $options['permission']['NotBlank'] = [];

    $options['action']['AllowedValues'] = array_keys(PermissionActionItem::allowedActionValues());

    $options['action']['NotBlank'] = [];

    $constraint_manager = \Drupal::typedDataManager()->getValidationConstraintManager();
    $constraints[] = $constraint_manager->create('ComplexData', $options);

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'permission' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ],
      'action' => [
        'type' => 'varchar',
        'length' => 25,
        'not null' => TRUE,
      ],
    ];

    $schema = [
      'columns' => $columns,
      'indexes' => [
        'permission' => ['permission'],
        'action' => ['action'],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $random = new Random();

    $values['permission'] = $random->sentences(2);
    $values['action'] = array_rand(self::allowedActionValues());

    return $values;
  }

  /**
   * Returns allowed values for 'action' sub-field.
   *
   * @return array
   *   The list of allowed values.
   */
  public static function allowedActionValues() {
    return [
      RoleChangeLog::PERMISSION_ADDED => t('Added'),
      RoleChangeLog::PERMISSION_REMOVED => t('Removed'),
    ];
  }

}
