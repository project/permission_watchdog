<?php declare(strict_types=1);

namespace Drupal\permission_watchdog\Plugin\Field\FieldFormatter;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\permission_watchdog\Entity\RoleChangeLog;
use Drupal\permission_watchdog\Plugin\Field\FieldType\PermissionActionItem;
use Drupal\user\PermissionHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'permission_action_default' formatter.
 *
 * @FieldFormatter(
 *   id = "permission_action_default",
 *   label = @Translation("Default"),
 *   field_types = {"permission_action"}
 * )
 */
class PermissionActionDefaultFormatter extends FormatterBase {

  /**
   * Permissions handler.
   *
   * @var \Drupal\user\PermissionHandlerInterface
   */
  protected PermissionHandlerInterface $permissionHandler;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('user.permissions'),
      $container->get('module_handler')
    );
  }

  /**
   * Constructs a formatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\user\PermissionHandlerInterface $permission_handler
   *   Permissions handler.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Module handler.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, PermissionHandlerInterface $permission_handler, ModuleHandlerInterface $module_handler) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->permissionHandler = $permission_handler;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $allowed_values = PermissionActionItem::allowedActionValues();
    $all_permissions = $this->permissionHandler->getPermissions();

    $element[0][RoleChangeLog::PERMISSION_ADDED] = [
      '#theme' => 'item_list',
      '#title' => $allowed_values[RoleChangeLog::PERMISSION_ADDED],
      '#list_type' => 'ul',
    ];
    $element[0][RoleChangeLog::PERMISSION_REMOVED] = [
      '#theme' => 'item_list',
      '#title' => $allowed_values[RoleChangeLog::PERMISSION_REMOVED],
      '#list_type' => 'ul',
    ];
    foreach ($items as $item) {
      $provider = $all_permissions[$item->permission]['provider'];
      $provider_name = $this->moduleHandler->getName($provider);
      $element[0][$item->action]['#items'][] = $this->t('<strong>@provider</strong>: @permission', [
        '@provider' => $provider_name,
        '@permission' => $item->permission,
      ]);
    }
    if (empty($element[0][RoleChangeLog::PERMISSION_ADDED]['#items'])) {
      unset($element[0][RoleChangeLog::PERMISSION_ADDED]);
    }
    if (empty($element[0][RoleChangeLog::PERMISSION_REMOVED]['#items'])) {
      unset($element[0][RoleChangeLog::PERMISSION_REMOVED]);
    }

    return $element;
  }

}
