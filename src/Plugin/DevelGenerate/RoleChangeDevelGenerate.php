<?php declare(strict_types=1);

namespace Drupal\permission_watchdog\Plugin\DevelGenerate;

use Drupal\Component\Datetime\Time;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\devel_generate\DevelGenerateBase;
use Drupal\permission_watchdog\Entity\RoleChangeLog;
use Drupal\user\PermissionHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a ContentDevelGenerate plugin.
 *
 * @DevelGenerate(
 *   id = "role_change",
 *   label = @Translation("Role change"),
 *   description = @Translation("Generate a given number of content. Optionally delete current content."),
 *   url = "role-change",
 *   permission = "administer devel_generate",
 *   settings = {
 *     "num" = 50,
 *     "kill" = FALSE,
 *     "max_actions" = 5,
 *   }
 * )
 */
class RoleChangeDevelGenerate extends DevelGenerateBase implements ContainerFactoryPluginInterface {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Provides system time.
   *
   * @var \Drupal\Core\Datetime\Time
   */
  protected $time;

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Role change storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $roleChangeStorage;

  /**
   * Permission handler.
   *
   * @var \Drupal\user\PermissionHandlerInterface
   */
  protected PermissionHandlerInterface $permissionHandler;

  /**
   * The construct.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $role_change_storage
   *   The role change storage.
   * @param \Drupal\user\PermissionHandlerInterface $permission_handler
   *   Permission handler.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Component\Datetime\Time $time
   *   Provides system time.
   * @param \Drupal\Core\Database\Connection $database
   *   Database connection.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, EntityStorageInterface $role_change_storage, PermissionHandlerInterface $permission_handler, DateFormatterInterface $date_formatter, Time $time, Connection $database) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->roleChangeStorage = $role_change_storage;
    $this->permissionHandler = $permission_handler;
    $this->dateFormatter = $date_formatter;
    $this->time = $time;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $entity_type_manager = $container->get('entity_type.manager');
    return new static(
      $configuration, $plugin_id, $plugin_definition,
      $entity_type_manager->getStorage('role_change_log'),
      $container->get('user.permissions'),
      $container->get('date.formatter'),
      $container->get('datetime.time'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $form['kill'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('<strong>Delete all changes</strong> before generating new ?'),
      '#default_value' => $this->getSetting('kill'),
    ];
    $form['num'] = [
      '#type' => 'number',
      '#title' => $this->t('How many changes would you like to generate?'),
      '#default_value' => $this->getSetting('num'),
      '#required' => TRUE,
      '#min' => 0,
    ];
    $form['max_actions'] = [
      '#type' => 'number',
      '#title' => $this->t('How many actions per change would you like to generate?'),
      '#default_value' => $this->getSetting('max_actions'),
      '#required' => TRUE,
      '#min' => 0,
    ];

    $options = [1 => $this->t('Now')];
    foreach ([3600, 86400, 604800, 2592000, 31536000] as $interval) {
      $options[$interval] = $this->dateFormatter->formatInterval($interval, 1) . ' ' . $this->t('ago');
    }
    $form['time_range'] = [
      '#type' => 'select',
      '#title' => $this->t('How far back in time should the changes be dated?'),
      '#description' => $this->t('Change creation dates will be distributed randomly from the current time, back to the selected time.'),
      '#options' => $options,
      '#default_value' => 604800,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function generateElements(array $values) {
    $actions = [
      RoleChangeLog::PERMISSION_ADDED,
      RoleChangeLog::PERMISSION_REMOVED,
    ];
    $roles = user_role_names();
    $permissions = $this->permissionHandler->getPermissions();
    if (!empty($values['kill'])) {
      $this->contentKill($values);
    }

    $query = $this->database->select('users', 'u')
      ->fields('u', ['uid'])
      ->range(0, 50)
      ->orderRandom();
    $authors = $query->execute()->fetchCol();

    for ($i = 1; $i <= $values['num']; $i++) {
      $uid = $authors[array_rand($authors)];
      $role = array_rand($roles);

      $change_values = [
        'uid' => $uid,
        'role' => $role,
        'timestamp' => $this->time->getRequestTime() - mt_rand(0, $values['time_range']),
      ];
      for ($j = 0; $j < rand(1, $values['max_actions']); $j++) {
        $change_values['actions'][] = [
          'action' => $actions[array_rand($actions)],
          'permission' => array_rand($permissions),
        ];
      }

      $change = $this->roleChangeStorage->create($change_values);
      $change->save();
    }
  }

  /**
   * Deletes all changes.
   */
  protected function contentKill() {
    $ids = $this->roleChangeStorage->getQuery()
      ->execute();

    if (!empty($ids)) {
      $changes = $this->roleChangeStorage->loadMultiple($ids);
      $this->roleChangeStorage->delete($changes);
      $this->setMessage($this->t('Deleted %count changes.', ['%count' => count($ids)]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateDrushParams(array $args, array $options = []) {

  }

}
