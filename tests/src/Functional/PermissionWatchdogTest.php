<?php declare(strict_types=1);

namespace Drupal\Tests\permission_watchdog\Functional;

use Drupal\permission_watchdog\Entity\RoleChangeLog;
use Drupal\Tests\BrowserTestBase;

/**
 * Test capturing changes to the role permissions in UI.
 *
 * @group permission_watchdog
 */
class PermissionWatchdogTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $profile = 'standard';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['permission_watchdog', 'media'];

  /**
   * Administrative user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $adminUser;

  /**
   * Change log storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->storage = $this->container->get('entity_type.manager')->getStorage('role_change_log');
    $changes = $this->storage->loadMultiple();
    $this->assertEquals(0, \count($changes), 'No changes logged');
    $this->adminUser = $this->drupalCreateUser(['administer permissions']);
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Test main form.
   */
  public function testUserPermissionsForm() {
    $this->drupalGet('admin/people/permissions');
    $change = [
      'authenticated[change own username]' => 1,
    ];
    $this->submitForm($change, 'Save permissions');
    $changes = $this->storage->loadByProperties(
      [
        'uid' => $this->adminUser->id(),
        'role' => 'authenticated',
      ]);
    $this->assertEquals(1, \count($changes), 'One change logged');
    $change = array_shift($changes);
    $this->assertEquals(RoleChangeLog::PERMISSION_ADDED, $change->actions[0]->action, 'Correct action logged');
    $this->assertEquals('change own username', $change->actions[0]->permission, 'Correct permission logged');
  }

  /**
   * Test module specific form.
   */
  public function testUserPermissionsModuleSpecificForm() {
    $this->drupalGet('admin/people/permissions/module/node');
    $change = [
      'content_editor[bypass node access]' => 1,
      'anonymous[access content]' => 0,
    ];
    $this->submitForm($change, 'Save permissions');
    $changes = $this->storage->loadMultiple();
    $this->assertEquals(2, \count($changes), 'Two changes logged');
    foreach ($changes as $change) {
      /** @var \Drupal\permission_watchdog\Entity\RoleChangeLog $change */
      $this->assertEquals($this->adminUser->id(), $change->uid->entity->id(), 'User ID match');
      $expectedRole = $change->role->entity->id() === 'content_editor' ? 'content_editor' : 'anonymous';
      $this->assertEquals($expectedRole, $change->role->entity->id(), 'Role match');
      $expectedAction = $change->actions[0]->action === RoleChangeLog::PERMISSION_ADDED ? RoleChangeLog::PERMISSION_ADDED : RoleChangeLog::PERMISSION_REMOVED;
      $this->assertEquals($expectedAction, $change->actions[0]->action, 'Action match');
      $expectedPermission = $change->actions[0]->permission === 'bypass node access' ? 'bypass node access' : 'access content';
      $this->assertEquals($expectedPermission, $change->actions[0]->permission, 'Permission match');
    }
  }

  /**
   * Test role specific form.
   */
  public function testUserPermissionsRoleSpecificForm() {
    $this->drupalGet('admin/people/permissions/content_editor');
    $change = [
      'content_editor[administer filters]' => 1,
    ];
    $this->submitForm($change, 'Save permissions');
    $changes = $this->storage->loadByProperties(
      [
        'uid' => $this->adminUser->id(),
        'role' => 'content_editor',
      ]);
    $this->assertEquals(1, \count($changes), 'One change logged');
    $change = array_shift($changes);
    $this->assertEquals(RoleChangeLog::PERMISSION_ADDED, $change->actions[0]->action, 'Correct action logged');
    $this->assertEquals('administer filters', $change->actions[0]->permission, 'Correct permission logged');
  }

  /**
   * Test node entity bundle form.
   *
   * Available on entity.node_type.entity_permissions_form route.
   */
  public function testEntityPermissionsFormNode() {
    $this->drupalGet('admin/structure/types/manage/page/permissions');
    $change = [
      'authenticated[delete any page content]' => 1,
    ];
    $this->submitForm($change, 'Save permissions');
    $changes = $this->storage->loadByProperties(
      [
        'uid' => $this->adminUser->id(),
        'role' => 'authenticated',
      ]);
    $this->assertEquals(1, \count($changes), 'One change logged');
    $change = array_shift($changes);
    $this->assertEquals(RoleChangeLog::PERMISSION_ADDED, $change->actions[0]->action, 'Correct action logged');
    $this->assertEquals('delete any page content', $change->actions[0]->permission, 'Correct permission logged');
  }

  /**
   * Test taxonomy term entity bundle form.
   *
   * Available on entity.taxonomy_vocabulary.entity_permissions_form route.
   */
  public function testEntityPermissionsFormTerm() {
    $this->drupalGet('admin/structure/taxonomy/manage/tags/overview/permissions');
    $change = [
      'content_editor[delete terms in tags]' => 1,
    ];
    $this->submitForm($change, 'Save permissions');
    $changes = $this->storage->loadByProperties(
      [
        'uid' => $this->adminUser->id(),
        'role' => 'content_editor',
      ]);
    $this->assertEquals(1, \count($changes), 'One change logged');
    $change = array_shift($changes);
    $this->assertEquals(RoleChangeLog::PERMISSION_ADDED, $change->actions[0]->action, 'Correct action logged');
    $this->assertEquals('delete terms in tags', $change->actions[0]->permission, 'Correct permission logged');
  }

  /**
   * Test media entity bundle form.
   *
   * Available on entity.media_type.entity_permissions_form route.
   */
  public function testEntityPermissionsFormMedia() {
    // Test media "document" form.
    $this->drupalGet('admin/structure/media/manage/document/permissions');
    $change = [
      'content_editor[delete any document media]' => 1,
    ];
    $this->submitForm($change, 'Save permissions');
    $changes = $this->storage->loadByProperties(
      [
        'uid' => $this->adminUser->id(),
        'role' => 'content_editor',
      ]);
    $this->assertEquals(1, \count($changes), 'One change logged');
    $change = array_shift($changes);
    $this->assertEquals(RoleChangeLog::PERMISSION_ADDED, $change->actions[0]->action, 'Correct action logged');
    $this->assertEquals('delete any document media', $change->actions[0]->permission, 'Correct permission logged');
  }

}
